# node.js messaging service API

API for get the conversations between two users with mongodb.

## Usage
```
npm install
```

```
npm start
```
or
```
nodemon
```

## Endpoints
To test the api, you can use Postman and create post/get requests using these adresses:
http://localhost:8080/endpoint

Specify a header `"Content-type" "application/json" ` and the body of the request.

```
POST /message // insert a message in mongodb database

body form:
{

}
```

```
GET /message/:id

```
