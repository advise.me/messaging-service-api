const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config/database');

mongoose.Promise = global.Promise;

mongoose.connect(config.database);

mongoose.connection.on('connected', () => {
  console.log('Connected to database' + config.database);
});

mongoose.connection.on('error', (err) => {
  console.log('Database error:' + err);
});

const app = express();

const conversations = require('./routes/conversations');

const port = process.env.PORT || 8091;

// CORS Middleware
app.use(cors());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json())

// Conversartions
app.use("/conversations", conversations);

// Start server
app.listen(port, () => {
  console.log('Server started on port ' + port)
});
