var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
const Conversation = require('../models/conversation');

server.listen(4000);

// socket io
io.on('connection', function (socket) {
  socket.on('disconnect', function() {
  });
  socket.on('save-message', function (data) {
    io.emit('new-message', { message: data });
  });
});

/* GET ALL CONVERSATIONS OF A USER */
router.get('/:username', (req, res, next) => {
  Conversation.find({ $or: [{user1: req.params.username }, {user2: req.params.username }]}, function (err, convs) {
    if (err) return next(err);
    res.json({success:true, msg:convs});
  });
});

/* GET A CONVERSATION BETWEEN TWO USERS */
router.put('/', (req, res, next) => {
  Conversation.getOneConversationByUsers(req.body.expeditor, req.body.recipient, (err, conv) => {
    if(err) {
      res.json({success: false, msg:err});
    } else {
      res.json({success: true, msg:conv});
    }
  });
});

/* SAVE CONVERSATION BETWEEN TO USERS */
router.post('/', (req, res, next) => {
  Conversation.addConversation(req.body.expeditor, req.body.recipient, req.body.message, (err, conv) => {
    if(err) {
      res.json({success: false, msg:err});
    } else {
      res.json({success: true, msg:conv});
    }
  });
});

module.exports = router;
