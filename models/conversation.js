const mongoose = require('mongoose');
const schema = mongoose.Schema;
const config = require('../config/database');

// Message schema
const MessageSchema = module.exports = mongoose.Schema({
  _id: {
    type: schema.Types.ObjectId
  },
  expeditor: {
    type:String,
    required:true
  },
  updated_at: {
    type: Date, default: Date.now
  },
  content: {
    type: String,
    required: true
  }
});

const Message = module.exports = mongoose.model('Message', MessageSchema);


// Conversation schema
const ConversationSchema = mongoose.Schema({
  _id: {
    type: schema.Types.ObjectId
  },
  user1: {
    type: String
  },
  user2: {
    type: String
  },
  messages : [MessageSchema]
});

const Conversation = module.exports = mongoose.model('Conversation', ConversationSchema);

module.exports.getAllConversationsOfUser = function(username, callback) {
  const query = { $or: [{user1: username }, {user2: username }]}
  Conversation.find(query, callback);
}

module.exports.getOneSideConversationByUsers = function(exp, rec, callback) {
  const query = {user1: exp, user2: rec}
  Conversation.findOne(query, callback);
}

module.exports.getOneConversationByUsers = function(exp, rec, callback) {
  Conversation.getOneSideConversationByUsers(exp, rec, (err, conv) => {
    if(err) throw err;
    if (conv) {
      callback(null, conv);
    } else {
      Conversation.getOneSideConversationByUsers(rec, exp, (errBis, convBis) => {
        if(errBis) throw errBis;
        if(convBis) {
          callback(null, convBis);
        } else {
          callback(null, null);
        }
      });
    }
  });
}

module.exports.addConversation = function(exp, rec, message, callback) {
  Conversation.getOneConversationByUsers(exp, rec, (err, conv) => {
    if(err) throw err;
    let newMessage = new Message({
      _id: new mongoose.Types.ObjectId,
      expeditor:exp,
      content: message
    });
    if (conv) {
      conv.messages.push(newMessage);
      conv.save(callback);
    } else {
      let newConv = new Conversation({
        _id: new mongoose.Types.ObjectId,
        user1: exp,
        user2: rec,
        messages: [newMessage]
      });
      newConv.save(callback);
    }
  });
}
